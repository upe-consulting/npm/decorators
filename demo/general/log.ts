/* tslint:disable:max-classes-per-file */
import { LogClass, LogMethod } from '../../src/general/log';

@LogClass()
export class LogDemo {
  public constructor(public parameter1: string, public parameter2: string) {}

  @LogMethod
  public foo(parameter1: string, parameter2: string): string {
    return parameter1 + parameter2 + this.parameter1 + this.parameter2;
  }

}

export class LogUseNormalConsoleDemo {

  public constructor(public parameter1: string, public parameter2: string) {}

  @LogMethod
  public foo(parameter1: string, parameter2: string): string {
    return parameter1 + parameter2 + this.parameter1 + this.parameter2;
  }

}
