import { Required, ValidateRequired } from '../../src/parameter/required';

export class RequiredDemo {

  public constructor(public parameter: string) {
  }

  @ValidateRequired
  public validateRequired(@Required param: string): number {
    return (param + this.parameter).length;
  }

  @ValidateRequired
  public validateOneRequired(@Required param1: string, param2: string): number {
    return (param1 + param2 + this.parameter).length;
  }

  // This throws an DecoratorNotAllowedError because not any parameter has an @Required decorator
  @ValidateRequired
  public validateRequiredThrow(param: string): number {
    return (param + this.parameter).length;
  }

}
