import { Sealed } from '../../src/class/sealed';

@Sealed
export class SealedDemo {
}

// tslint:disable:max-classes-per-file
export class InheredSealedDemo extends SealedDemo {
}
