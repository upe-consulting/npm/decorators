import { Singleton } from '../../src/design-patterns/singleton';

@Singleton
export class SingletonDemo {
  public constructor(public parameter: string) {}
}
