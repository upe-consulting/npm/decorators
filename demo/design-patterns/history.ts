import { HistoryMember, HistoryObject, IHistory } from '../../src/design-patterns/history';

@HistoryObject
export class HistoryDemo implements IHistory {
  public get member(): string {
    return this._member;
  }

  @HistoryMember('_member')
  public set member(value: string) {
    this._member = value;
  }

  private _member: string;

  public cleanHistory(): void {}

  public redo(): void {}

  public undo(): void {}
}
