/* tslint:disable:prefer-function-over-method */
import { IRevisionLite, RevisionLiteObject } from '../../src/design-patterns/revision-lite';

@RevisionLiteObject
export class RevisionLiteDemo implements IRevisionLite {
  public allVersions: RevisionLiteDemo[];
  public member1: string;
  public member2: string;
  public version: number;

  public createRevision(): void {
  }

  public deserialize(json: IRevisionLite): void {
    Object.keys(json).forEach((key: string) => {
      this[ key ] = json[ key ];
    });
  }

  public getVersion(version: number): RevisionLiteDemo | null {
    return null;
  }

  public revertTo(version: number): boolean {
    return true;
  }

  public serialize(): IRevisionLite {
    return JSON.parse(JSON.stringify(this));
  }

}
