/* tslint:disable:prefer-function-over-method */

import { IRevision, RevisionObject } from '../../src/design-patterns/revision';

@RevisionObject
export class RevisionDemo implements IRevision {

  public followingVersion: RevisionDemo;
  public member1: string;
  public member2: string;
  public previousVersion: RevisionDemo;
  public version: number;

  public createRevision(): void {
  }

  public deserialize(json: IRevision): void {
    Object.keys(json).forEach((key: string) => {
      this[ key ] = json[ key ];
    });
  }

  public getAllFollowingVersions(): RevisionDemo[] {
    return [];
  }

  public getAllPreviousVersions(): RevisionDemo[] {
    return [];
  }

  public getAllVersions(): RevisionDemo[] {
    return [];
  }

  public getVersion(revision: number): RevisionDemo | null {
    return null;
  }

  public revertTo(revision: number): boolean {
    return true;
  }

  public serialize(): IRevision {
    return JSON.parse(JSON.stringify(this));
  }

}
