[![build status](https://gitlab.com/upe-consulting/npm/decorators/badges/master/build.svg)](https://gitlab.com/upe-consulting/npm/decorator/commits/master)
[![coverage report](https://gitlab.com/upe-consulting/npm/decorators/badges/master/coverage.svg)](https://gitlab.com/upe-consulting/npm/decorator/commits/master)

# Decorators

+ [Class](#class)
  + [Sealed](#sealed)
+ [Design Patterns](#design-patterns)
  + [History](#history)
  + [Revision](#revision)
  + [Revision Lite](#revision-lite)
+ [General](#general)
  + [Log](#log)
    + [Class](#log-class)
    + [Method](#log-method)

## Class

### Sealed

If the `Sealed` decorator is added to an class, this class cannot be inherited any more. If a class try to inherit this class an error will be thrown.

```typescript
import { Sealed } from '@upe/decocators';

@Sealed
export class SealedDemo { }
```

## Design Patterns

### History

```typescript
import { HistoryMember, HistoryObject, IHistory } from '@upe/decocators';

@HistoryObject
export class HistoryDemo implements IHistory {
  public get member(): string {
    return this._member;
  }

  @HistoryMember('_member')
  public set member(value: string) {
    this._member = value;
  }

  private _member: string;

  public cleanHistory(): void {}

  public redo(): void {}

  public undo(): void {}
}
```

### Revision

```typescript
import { IRevision, RevisionObject } from '@upe/decocators';

@RevisionObject
export class RevisionDemo implements IRevision {

  public followingVersion: RevisionDemo;
  public member1: string;
  public member2: string;
  public previousVersion: RevisionDemo;
  public version: number;

  public createRevision(): void {
  }

  public deserialize(json: IRevision): void {
    Object.keys(json).forEach((key: string) => {
      this[ key ] = json[ key ];
    });
  }

  public getAllFollowingVersions(): RevisionDemo[] {
    return [];
  }

  public getAllPreviousVersions(): RevisionDemo[] {
    return [];
  }

  public getAllVersions(): RevisionDemo[] {
    return [];
  }

  public getVersion(revision: number): RevisionDemo | null {
    return null;
  }

  public revertTo(revision: number): boolean {
    return true;
  }

  public serialize(): IRevision {
    return JSON.parse(JSON.stringify(this));
  }

}
```

### Revision Lite

```typescript
import { IRevisionLite, RevisionLiteObject } from '@upe/decocators';

@RevisionLiteObject
export class RevisionLiteDemo implements IRevisionLite {
  public allVersions: RevisionLiteDemo[];
  public member1: string;
  public member2: string;
  public version: number;

  public createRevision(): void {
  }

  public deserialize(json: IRevisionLite): void {
    Object.keys(json).forEach((key: string) => {
      this[ key ] = json[ key ];
    });
  }

  public getVersion(version: number): RevisionLiteDemo | null {
    return null;
  }

  public revertTo(version: number): boolean {
    return true;
  }

  public serialize(): IRevisionLite {
    return JSON.parse(JSON.stringify(this));
  }

}
```

## General

### Log

#### Log Class

If a class with the `LogClass` decorator is created a console log info will be printed to the console.
 
```typescript
import { LogClass } from '@upe/decocators';

@LogClass()
export class LogDemo {
  public constructor(public parameter1: string, public parameter2: string) {}

}
```

#### Log Method

If a method with the `LogMethod` decorator is called, the paramter and the return type of this method will be printed to the console.

```typescript
import { LogClass, LogMethod } from '@upe/decocators';

@LogClass()
export class LogDemo {
  public constructor(public parameter1: string, public parameter2: string) {}

  @LogMethod
  public foo(parameter1: string, parameter2: string): string {
    return parameter1 + parameter2 + this.parameter1 + this.parameter2;
  }

}
```

