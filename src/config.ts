export interface IConfig {
  console: Console;
  undoMetaFieldKey: string;
}

export let CONFIG: IConfig = {
  // tslint:disable:object-literal-shorthand
  console:          console,
  undoMetaFieldKey: '__typedJsonJsonObjectMetadataInformation__',
};
