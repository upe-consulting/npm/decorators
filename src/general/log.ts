import { Level, Log } from 'typescript-logger';
import { Logger } from 'typescript-logger/build/logger';
import { CONFIG } from '../config';

export interface ILogClassSettings {
  logLevel?: Level[];
  silent?: boolean;
}

export interface ILoggable {
  logger: Logger<any>;
}

export function LogClass(settings: ILogClassSettings = {}): any {
  return <T extends { new(...args: any[]): ILoggable }>(constructor: T): any =>
    class extends constructor implements ILoggable {

      public constructor(...args: any[]) {
        super(...args);
        if (!this.logger) {
          // TODO : add util get class name;
          const nameProperty = 'name';
          const name         = constructor[ nameProperty ] ? constructor[ nameProperty ] : 'any';
          if (!settings.logLevel) {
            settings.logLevel = [];
          }
          this.logger = Log.create(CONFIG.console, name, ...settings.logLevel);
        }
        if (!settings.silent) {
          this.logger.i('new instance created', args);
        }
      }
    };
}

export function LogMethod(target: object, key: string, descriptor: any): any {
  // save a reference to the original method this way we keep the values currently in the
  // descriptor and don't overwrite what another decorator might have done to the descriptor.
  if (descriptor === undefined) {
    descriptor = Object.getOwnPropertyDescriptor(target, key);
  }
  const originalMethod = descriptor.value;

  // editing the descriptor/value parameter
  descriptor.value = function (): any {
    const args: any[] = [];
    for (let i = 0; i < arguments.length; i++) {
      args[ i ] = arguments[ i ];
    }

    // note usage of originalMethod here
    const result = originalMethod.apply(this, args);

    const argsStringify  = args.map((arg: any) => JSON.stringify(arg)).join();
    const resultStrigify = JSON.stringify(result);

    if (this.hasOwnProperty('logger') && this.logger instanceof Logger) {
      this.logger.i(`Call: ${key}() => ${resultStrigify}`, args);
    } else {
      CONFIG.console.log(`Call: ${key}(${argsStringify}) => ${resultStrigify}`);
    }

    return result;
  };

  // return edited descriptor as opposed to overwriting the descriptor
  return descriptor;
}
