export class ParameterRequiredError extends Error {

  public constructor(functionName: string = 'Unknown') {
    const msg = `Called function '${functionName}' has required parameter/s!`;
    super(msg);

    this.name    = 'ParameterRequiredError';
    this.message = msg;
    this.stack   = (new Error() as any).stack;
  }

}
