/* tslint:disable:max-classes-per-file */

export class DecoratorError extends Error {
  public constructor(decorator: string = 'Unknown') {
    super(decorator);

    this.name    = 'DecoratorError';
    this.message = decorator;
    this.stack   = (new Error() as any).stack;
  }
}

export class DecoratorNotAllowedError extends DecoratorError {

  public constructor(decorator?: string) {
    super(`The use of the decorator '${decorator}' is not allowed in this way!`);

    this.name  = 'DecoratorNotAllowedError';
    this.stack = (new DecoratorError() as any).stack;
  }

}

export class DecoratorMissUsageError extends Error {

  public static MissingProperty(
    decorator: string,
    propertyName: string,
    parentDecorator?: string): DecoratorMissUsageError {
    let msg = `Cannot find '${propertyName}' property.`;
    if (parentDecorator) {
      msg += `Is the ${parentDecorator} decorator added to this class?`;
    }

    return new DecoratorMissUsageError('@HistoryMember', msg);
  }

  public constructor(decorator: string = 'Unknown', message: string = 'Unknown error') {
    super(`'${decorator}' has thrown '${message}' as error!`);

    this.name  = 'DecoratorMissUsageError';
    this.stack = (new DecoratorError() as any).stack;
  }

}
