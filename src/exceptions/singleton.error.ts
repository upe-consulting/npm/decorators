export class SingletonError extends Error {

  public constructor(className: string = 'Unknown') {
    const msg = `Can not create multiple instances of ${className}, because this class is a singleton!`;
    super(msg);

    this.name    = 'SingletonError';
    this.message = msg;
    this.stack   = (new Error() as any).stack;
  }

}
