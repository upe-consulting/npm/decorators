export class HistoryError extends Error {

  public constructor(message?: string) {
    super(message);

    this.name = 'HistoryError';
    if (message) {
      this.message = message;
    }
    this.stack = (new Error() as any).stack;
  }

}
