export interface ISerializable<T> {
  deserialize(json: T): void;
  serialize(): T;
}
