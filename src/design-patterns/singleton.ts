import { construct } from '../class/utility';
import { SingletonError } from '../exceptions/singleton.error';

export function Singleton(target: any): any {

  // save a reference to the original constructor
  const original = target;

  // tslint:disable:only-arrow-functions
  // the new constructor behaviour
  const f: any = function (...args: any[]): any {

    original.prototype.test = 'var';

    // check if class was already instanced
    if (original.prototype._created) {
      throw new SingletonError(original.name);
    } else {
      original.prototype._created = true;
      // save current instance in static property
      f.Instance                  = this;
    }

    return construct(original, args);
  };

  // copy prototype so intanceof operator still works
  f.prototype = original.prototype;

  // return new constructor (will override original)
  return f;
}
