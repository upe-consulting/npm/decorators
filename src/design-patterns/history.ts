/* tslint:disable:max-classes-per-file */
import { DecoratorMissUsageError } from '../exceptions/decorator.error';
import { HistoryError } from '../exceptions/history.error';

export interface IHistory {
  cleanHistory(): void;
  redo(): void;
  undo(): void;
}

export class PropertyHistory {

  public get currentPos(): number {
    return this._currentPos;
  }

  public get entries(): PropertyHistoryEntry[] {
    return this._entries;
  }

  private _currentPos: number = -1;

  private _entries: PropertyHistoryEntry[] = [];

  public add(entry: PropertyHistoryEntry): void {
    if (this._currentPos !== this.entries.length - 1) {
      this.entries.splice(this._currentPos + 1, this.entries.length - 1 - this._currentPos);
    }
    this._entries.push(entry);
    this._currentPos++;
  }

  public clean(): void {
    this._entries    = [];
    this._currentPos = -1;
  }

  public hasNext(): boolean {
    return this._currentPos < this._entries.length - 1;
  }

  public hasPrevious(): boolean {
    return this._currentPos >= 0;
  }

  public next(): PropertyHistoryEntry {
    if (this._entries.length === 0) {
      throw new HistoryError('Cannot get next history entry. There is not any entry in the history!');
    }

    if (this._currentPos === this._entries.length - 1) {
      throw new HistoryError('Cannot get next history entry. ' +
                             'Current pos is equals then max pos of the history entry array!');
    }

    return this._entries[ ++this._currentPos ];
  }

  public previous(): PropertyHistoryEntry {
    if (this._entries.length === 0) {
      throw new HistoryError('Cannot get previous history entry. There is not any entry in the history!');
    }

    if (this._currentPos < 0) {
      throw new HistoryError('Cannot get previous history entry. Current pos is less or equal then 0!');
    }

    return this._entries[ --this._currentPos ];
  }

}

export class PropertyHistoryEntry {
  public constructor(public memberName: string, public value: any) {
  }
}

export const _propertyHistory            = '_propertyHistory';
export const _enablePropertyHistory      = '_enablePropertyHistory';
export const _onceDisablePropertyHistory = '_onceDisablePropertyHistory';
export const cleanHistory                = 'cleanHistory';
export const undo                        = 'undo';
export const redo                        = 'redo';

export function HistoryMember(field: string): any {

  // tslint:disable:only-arrow-functions
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor): void {
    const set = descriptor.set;

    if (set) {
      descriptor.set = function (value: any): void {
        if (!(this[ _propertyHistory ] instanceof PropertyHistory)) {
          throw DecoratorMissUsageError.MissingProperty('@HistoryMember', _propertyHistory, '@HistoryObject');
        }
        if (this[ _enablePropertyHistory ] === true) {
          if (this[ _onceDisablePropertyHistory ] === false) {
            this[ _propertyHistory ].add(new PropertyHistoryEntry(field, value));
          } else if (this[ _onceDisablePropertyHistory ] === true) {
            this[ _onceDisablePropertyHistory ] = false;
          } else {
            throw DecoratorMissUsageError.MissingProperty(
              '@HistoryMember',
              _onceDisablePropertyHistory,
              '@HistoryObject',
            );
          }
        } else if (this[ _enablePropertyHistory ] !== false) {
          throw DecoratorMissUsageError.MissingProperty('@HistoryMember', _enablePropertyHistory, '@HistoryObject');
        }
        set.apply(this, [ value ]);
      };
    }

    Object.defineProperty(target, propertyKey, descriptor);
  };
}

export function HistoryObject<T extends { new(...args: any[]): {} }>(constructor: T): any {

  return class extends constructor {
    // tslint:disable:no-unused-variable
    private _enablePropertyHistory: boolean = true;

    private _onceDisablePropertyHistory: boolean = false;

    private _propertyHistory: PropertyHistory = new PropertyHistory();

    public cleanHistory(): void {
      super[ cleanHistory ]();
      this._propertyHistory.clean();
    }

    public redo(): void {
      super[ redo ]();
      if (this._propertyHistory.hasNext()) {
        const entry                      = this._propertyHistory.next();
        this._onceDisablePropertyHistory = true;
        this[ entry.memberName ]         = entry.value;
      }
    }

    public undo(): void {
      super[ undo ]();
      if (this._propertyHistory.hasPrevious()) {
        const entry                      = this._propertyHistory.previous();
        this._onceDisablePropertyHistory = true;
        this[ entry.memberName ]         = entry.value;
      }
    }

  };
}
