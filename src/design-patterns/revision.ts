import { ISerializable } from '../interfaceses';

export const _followingVersion = '_followingVersion';
export const _previousVersion  = '_previousVersion';

export interface IRevision extends ISerializable<IRevision> {
  followingVersion: IRevision | null;
  previousVersion: IRevision | null;
  version: number;

  createRevision(): void;
  getAllFollowingVersions(): IRevision[];
  getAllPreviousVersions(): IRevision[];
  getAllVersions(): IRevision[];
  getVersion(revision: number): IRevision | null;
  revertTo(revision: number): boolean;
}

export function RevisionObject<T extends { new(...args: any[]): IRevision }>(constructor: T): any {

  // tslint:disable:variable-name
  const Revision = class extends constructor implements IRevision {

    public constructor(...args: any[]) {
      super(...args);
      if (!this.version) {
        this.version = 1;
      }
      if (!this.previousVersion) {
        this.previousVersion = null;
      }
      if (!this.followingVersion) {
        this.followingVersion = null;
      }
    }

    public createRevision(): void {
      const json: IRevision = this.serialize();
      const revision        = new Revision();
      revision.deserialize(json);
      this.version++;
      if (this.previousVersion) {
        this.previousVersion.followingVersion = revision;
        revision.previousVersion              = this.previousVersion;
      }
      this.previousVersion      = revision;
      revision.followingVersion = this;
    }

    public getAllFollowingVersions(): IRevision[] {
      if (this.followingVersion !== null) {
        return [ this.followingVersion ].concat(this.followingVersion.getAllFollowingVersions());
      } else {
        return [];
      }
    }

    public getAllPreviousVersions(): IRevision[] {
      if (this.previousVersion !== null) {
        return this.previousVersion.getAllPreviousVersions().concat([ this.previousVersion ]);
      } else {
        return [];
      }
    }

    public getAllVersions(): IRevision[] {
      const pre                  = this.getAllPreviousVersions();
      const next                 = this.getAllFollowingVersions();
      const current: IRevision[] = [ this ];

      return pre.concat(current.concat(next));
    }

    public getVersion(revision: number): IRevision | null {
      super.getVersion(revision);
      if (revision <= 0) {
        throw new Error();
      }
      if (this.version === revision) {
        return this;
      } else if (this.version > revision && this.previousVersion !== null) {
        return this.previousVersion.getVersion(revision);
      } else if (this.followingVersion !== null) {
        return this.followingVersion.getVersion(revision);
      }

      return null;
    }

    public revertTo(revision: number): boolean {
      if (super.revertTo(revision)) {
        if (revision !== this.version) {
          const version = this.getVersion(revision);
          if (version) {
            this.createRevision();
            const json = version.serialize();
            delete json[ _previousVersion ];
            delete json[ _followingVersion ];
            const oldRevsion = this.version;
            this.deserialize(version.serialize());
            this.version = oldRevsion;

            return true;
          }
        }
      }

      return false;
    }

    /**
     * Prevent that the given serializer try to serialize a circle structure.
     */
    public serialize(): IRevision {
      let tmpFollowingVersion: IRevision | null = null;
      if (this.followingVersion) {
        tmpFollowingVersion   = this.followingVersion;
        this.followingVersion = null;
      }
      let tmpPreviousVersion: IRevision | null = null;
      if (this.previousVersion) {
        tmpPreviousVersion   = this.previousVersion;
        this.previousVersion = null;
      }
      const json            = super.serialize();
      this.followingVersion = tmpFollowingVersion;
      this.previousVersion  = tmpPreviousVersion;

      return json;
    }

  };

  return Revision;

}
