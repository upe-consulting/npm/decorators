import { ISerializable } from '../interfaceses';

export interface IRevisionLite extends ISerializable<IRevisionLite> {
  allVersions: IRevisionLite[];
  version: number;

  createRevision(): void;
  getVersion(version: number): IRevisionLite | null;
  revertTo(version: number): boolean;
}

export function RevisionLiteObject<T extends { new(...args: any[]): IRevisionLite }>(constructor: T): any {

  // tslint:disable:variable-name
  const Revision = class extends constructor implements IRevisionLite {

    public constructor(...args: any[]) {
      super(...args);
      if (!this.version) {
        this.version = 1;
      }
      if (!this.allVersions) {
        this.allVersions = [ this ];
      } else {
        this.allVersions.push(this);
      }
    }

    public createRevision(): void {
      const json: IRevisionLite = this.serialize();
      const revision            = new Revision();
      revision.deserialize(json);
      this.version++;
      this.allVersions.push(revision);
    }

    public getVersion(version: number): IRevisionLite | null {
      const find = this.allVersions.filter((versionObj: IRevisionLite) => versionObj.version === version)[ 0 ];

      return !find ? null : find;
    }

    public revertTo(version: number): boolean {
      if (super.revertTo(version)) {
        if (version !== this.version) {
          const versionObj = this.getVersion(version);
          if (versionObj !== null) {
            this.createRevision();
            const json       = versionObj.serialize();
            const oldRevsion = this.version;
            this.deserialize(json);
            this.version = oldRevsion;

            return true;
          }
        }
      }

      return false;
    }

    public serialize(): IRevisionLite {
      const tmpAllVersions = this.allVersions;
      this.allVersions     = [];
      const json           = super.serialize();
      this.allVersions     = tmpAllVersions;

      return json;
    }

  };

  return Revision;
}
