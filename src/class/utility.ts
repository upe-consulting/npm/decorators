// a utility function to generate instances of a class
export function construct(constructor: new () => any, args: any): any {
  const c: any = function (): any {
    return constructor.apply(this, args);
  };
  c.prototype  = constructor.prototype;

  return new c();
}
