import 'reflect-metadata';
import { DecoratorNotAllowedError } from '../exceptions/decorator.error';
import { ParameterRequiredError } from '../exceptions/required.error';

declare function Symbol(val: string): any;

const requiredMetadataKey = Symbol('required');

export function Required(target: object, propertyKey: string | symbol, parameterIndex: number): void {
  const existingRequiredParameters: number[] = Reflect.getOwnMetadata(requiredMetadataKey, target, propertyKey) || [];
  existingRequiredParameters.push(parameterIndex);
  Reflect.defineMetadata(requiredMetadataKey, existingRequiredParameters, target, propertyKey);
}

// tslint:disable:ban-types
export function ValidateRequired(
  target: any, propertyName: string,
  descriptor: TypedPropertyDescriptor<Function>): void {
  const method = descriptor.value;
  // tslint:disable:only-arrow-functions
  descriptor.value = function (): any {
    const requiredParameters: number[] = Reflect.getOwnMetadata(requiredMetadataKey, target, propertyName);
    if (requiredParameters) {
      for (const parameterIndex of requiredParameters) {
        if (parameterIndex >= arguments.length || arguments[ parameterIndex ] === undefined) {
          throw new ParameterRequiredError(propertyName);
        }
      }
    } else {
      throw new DecoratorNotAllowedError('@ValidateRequired');
    }
    if (!method) {
      // TODO : add/import undefined error
      throw new Error('Undefined');
    }

    return method.apply(this, arguments);
  };
}
