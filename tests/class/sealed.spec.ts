/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { InheredSealedDemo, SealedDemo } from '../../demo/class/sealed';

@suite
class SealedTest {

  @test '@Sealed'(): void {
    expect(() => new SealedDemo()).to.not.throw;
    expect(() => new InheredSealedDemo()).to.throw;
  }

}
