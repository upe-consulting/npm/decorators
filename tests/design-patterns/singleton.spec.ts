/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
/* tslint:disable:no-string-literal */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { SingletonDemo } from '../../demo/design-patterns/singleton';

@suite
class SingletonTest {

  @test '@Singleton'(): void {

    const singleton = new SingletonDemo('parameter');

    expect(singleton).to.be.instanceof(SingletonDemo);

    expect(singleton.parameter).to.be.equal('parameter');

    expect(SingletonDemo[ 'prototype' ][ '_created' ]).to.be.not.undefined;

    expect(SingletonDemo[ 'prototype' ][ '_created' ]).to.be.true;

    expect(SingletonDemo[ 'Instance' ]).to.be.not.undefined;

    expect(SingletonDemo[ 'Instance' ]).to.be.instanceof(SingletonDemo);

    expect(() => new SingletonDemo('parameter')).to.throw(/class is a singleton/);

  }

}
