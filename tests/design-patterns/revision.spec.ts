/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { RevisionDemo } from '../../demo/design-patterns/revision';

@suite
class RevisionTest {

  @test
  public 'constrcution test'(): void {
    const rd = new RevisionDemo();

    expect(rd).to.be.instanceof(RevisionDemo);
  }

  @test
  public 'create revison'(): void {

    const rd = new RevisionDemo();

    expect(rd.followingVersion).to.be.null;
    expect(rd.previousVersion).to.be.null;
    expect(rd.getAllVersions()).has.length(1);
    expect(rd.getAllVersions()[ 0 ]).to.be.instanceof(RevisionDemo);
    expect(rd.getAllVersions()[ 0 ]).to.be.deep.equal(rd);
    expect(rd.getAllVersions()[ 0 ].version).to.be.equal(1);
    expect(rd.getAllFollowingVersions()).to.be.empty;
    expect(rd.getAllPreviousVersions()).to.be.empty;
    expect(rd.getVersion(1)).to.be.not.null;
    expect(rd.getVersion(1)).to.be.instanceof(RevisionDemo);
    expect(rd.getVersion(2)).to.be.null;
    expect(() => rd.getVersion(0)).to.be.throw;
    expect(() => rd.getVersion(-1)).to.be.throw;
    expect(rd.revertTo(1)).to.be.false;
    expect(rd.revertTo(2)).to.be.false;
    expect(() => rd.revertTo(0)).to.throw;
    expect(() => rd.revertTo(-1)).to.throw;

    rd.createRevision();

    expect(rd.previousVersion).to.be.not.null;
    expect(rd.previousVersion).to.be.instanceof(RevisionDemo);
    expect(rd.getAllVersions()).has.length(2);

  }

  @test
  public 'revertTo previous version'() {

    const rd   = new RevisionDemo();
    rd.member1 = 'member1';
    rd.member2 = 'member2';
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();

    expect(rd.version).to.be.eq(5);
    expect(rd.member1).to.be.eq('member1');
    expect(rd.member2).to.be.eq('member2');

    rd.member1 = 'new-member1';
    rd.member2 = 'new-member2';

    expect(rd.revertTo(3)).to.be.true;

    expect(rd.version).to.be.eq(6);
    expect(rd.member1).to.be.eq('member1');
    expect(rd.member2).to.be.eq('member2');
  }

  @test
  public 'get previous version'() {

    const rd   = new RevisionDemo();
    rd.member1 = 'member1';
    rd.member2 = 'member2';
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();

    expect(rd.version).to.be.eq(5);
    expect(rd.member1).to.be.eq('member1');
    expect(rd.member2).to.be.eq('member2');

    rd.member1 = 'new-member1';
    rd.member2 = 'new-member2';

    const previous = rd.previousVersion;
    if (!previous) { throw new Error(); } // compiler workaround

    expect(previous.version).to.be.eq(4);
    expect(previous.member1).to.be.eq('member1');
    expect(previous.member2).to.be.eq('member2');

  }

  @test
  public 'get version by nummber'() {

    const rd   = new RevisionDemo();
    rd.member1 = 'member1';
    rd.member2 = 'member2';
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();

    expect(rd.version).to.be.eq(5);
    expect(rd.member1).to.be.eq('member1');
    expect(rd.member2).to.be.eq('member2');

    rd.member1 = 'new-member1';
    rd.member2 = 'new-member2';

    rd.createRevision();

    rd.member1 = 'next-member1';
    rd.member2 = 'next-member2';

    const version = rd.getVersion(3) as RevisionDemo;

    expect(version).to.be.not.undefined;
    expect(version).to.be.instanceof(RevisionDemo);
    expect(version.version).to.be.eq(3);
    expect(version.member1).to.be.eq('member1');
    expect(version.member2).to.be.eq('member2');

  }

  @test
  public 'get next version'() {

    const rd   = new RevisionDemo();
    rd.member1 = 'member1';
    rd.member2 = 'member2';
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();

    expect(rd.version).to.be.eq(5);
    expect(rd.member1).to.be.eq('member1');
    expect(rd.member2).to.be.eq('member2');

    rd.member1 = 'new-member1';
    rd.member2 = 'new-member2';

    rd.createRevision();
    rd.createRevision();
    rd.createRevision();
    rd.createRevision();

    rd.member1 = 'next-member1';
    rd.member2 = 'next-member2';

    const previous = rd.getVersion(7);
    if (!previous) { throw new Error(); } // compiler workaround

    expect(previous.followingVersion).to.be.not.undefined;
    expect(previous.followingVersion).to.be.not.null;
    expect(previous.followingVersion).to.be.instanceof(RevisionDemo);
    const next = previous.followingVersion;
    expect(next.version).to.be.eq(8);
    expect(next.member1).to.be.eq('new-member1');
    expect(next.member2).to.be.eq('new-member2');

  }

  @test
  public 'get all following versions'() {

    const rd                 = new RevisionDemo();
    const createVersionCount = 20;
    for (let i = 0; i < createVersionCount; i++) {
      rd.createRevision();
    }

    for (let versionNumber = 1; versionNumber < createVersionCount + 1; versionNumber++) {

      const version = rd.getVersion(versionNumber);

      if (!version) { throw new Error(); } // compiler workaround

      const followingVersions = version.getAllFollowingVersions();

      expect(followingVersions).has.length(createVersionCount + 1 - versionNumber);

      for (let i = 0; i < followingVersions.length; i++) {
        expect(followingVersions[ i ].version).eq(versionNumber + i + 1);
      }

    }

  }

  @test
  public 'get all previous versions'() {

    const rd                 = new RevisionDemo();
    const createVersionCount = 20;
    for (let i = 0; i < createVersionCount; i++) {
      rd.createRevision();
    }
    const createdVersions = createVersionCount + 1;

    for (let versionNumber = 1; versionNumber < createdVersions; versionNumber++) {

      const version = rd.getVersion(versionNumber);

      if (!version) { throw new Error(); } // compiler workaround

      const previousVersions = version.getAllPreviousVersions();

      expect(previousVersions).has.length(versionNumber - 1);

      for (let i = 0; i < previousVersions.length; i++) {
        expect(previousVersions[ i ].version).eq(i + 1);
      }

    }

  }

  @test
  public 'get all versions'(): void {

    const rd = new RevisionDemo();

    expect(rd.getAllVersions()).has.length(1);

    const createVersionCount = 20;
    for (let i = 0; i < createVersionCount; i++) {
      rd.createRevision();
    }
    const createdVersions = createVersionCount + 1;

    for (let versionNumber = 1; versionNumber < createdVersions; versionNumber++) {

      const version = rd.getVersion(versionNumber);

      if (!version) { throw new Error(); } // compiler workaround

      const versions = version.getAllVersions();

      expect(versions).has.length(createdVersions);

      for (let i = 0; i < versions.length; i++) {
        expect(versions[ i ].version).eq(i + 1);
      }

    }

  }

  @test
  public 'serialize'(): void {
    const rd = new RevisionDemo();

    const json = rd.serialize();

    expect(json[ 'followingVersion' ]).to.be.null;
    expect(json[ 'previousVersion' ]).to.be.null;
  }

}
