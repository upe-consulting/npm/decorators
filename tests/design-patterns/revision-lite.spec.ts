/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { RevisionLiteDemo } from '../../demo/design-patterns/revision-lite';

@suite
class RevisionLiteTest {

  @test
  public 'constrcution test'(): void {
    const rd = new RevisionLiteDemo();

    expect(rd).to.be.instanceof(RevisionLiteDemo);
  }

  @test
  public 'create revison'(): void {

    const rld = new RevisionLiteDemo();

    expect(rld.allVersions).has.length(1);
    expect(rld.getVersion(1)).to.be.not.null;
    expect(rld.getVersion(1)).to.be.instanceof(RevisionLiteDemo);
    expect(rld.getVersion(2)).to.be.null;
    expect(() => rld.getVersion(0)).to.be.throw;
    expect(() => rld.getVersion(-1)).to.be.throw;
    expect(rld.revertTo(1)).to.be.false;
    expect(rld.revertTo(2)).to.be.false;
    expect(() => rld.revertTo(0)).to.throw;
    expect(() => rld.revertTo(-1)).to.throw;

    rld.createRevision();

    expect(rld.allVersions).has.length(2);
    expect(rld.getVersion(1)).to.be.instanceof(RevisionLiteDemo);

  }

  @test
  public 'revertTo previous version'() {

    const rld   = new RevisionLiteDemo();
    rld.member1 = 'member1';
    rld.member2 = 'member2';
    rld.createRevision();

    expect(rld.version).to.be.eq(2);
    expect(rld.member1).to.be.eq('member1');
    expect(rld.member2).to.be.eq('member2');

    rld.member1 = 'new-member1';
    rld.member2 = 'new-member2';

    expect(rld.revertTo(1)).to.be.true;

    expect(rld.version).to.be.eq(3);
    expect(rld.member1).to.be.eq('member1');
    expect(rld.member2).to.be.eq('member2');
  }

}
