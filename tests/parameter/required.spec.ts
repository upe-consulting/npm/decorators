/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { RequiredDemo } from '../../demo/parameter/required';

@suite
class RequiredTest {

  @test '@Required only one required parameter'(): void {

    const parameter = 'parameter';
    const param     = 'param';

    const rd = new RequiredDemo(parameter);

    expect(() => rd.validateRequired(param)).to.not.throw;

    expect(rd.validateRequired(param)).to.be.equal((parameter + param).length);

    // tslint:disable:no-eval
    expect(() => eval('rd.validateRequired()'))
      .to.throw(`Called function 'validateRequired' has required parameter/s!`);

  }

  @test '@Required one required parameter'(): void {

    const parameter = 'parameter';
    const param1    = 'param1';
    const param2    = 'param2';

    const rd = new RequiredDemo(parameter);

    expect(() => rd.validateOneRequired(param1, param2)).to.not.throw;

    expect(rd.validateOneRequired(param1, param2)).to.be.equal((parameter + param1 + param2).length);

    // tslint:disable:no-eval
    expect(() => eval('rd.validateOneRequired()'))
      .to.throw(`Called function 'validateOneRequired' has required parameter/s!`);

  }

  @test '@ValidateRequired not any required parameter'(): void {

    const parameter = 'parameter';
    const param     = 'param';

    const rd = new RequiredDemo(parameter);

    expect(() => rd.validateRequiredThrow(param))
      .to.throw(`The use of the decorator '@ValidateRequired' is not allowed in this way!`);

  }

}
