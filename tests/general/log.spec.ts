/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import * as sinon from 'sinon';
import { LogDemo, LogUseNormalConsoleDemo } from '../../demo/general/log';
import { CONFIG } from '../../src/config';

@suite
class LogClassTest {

  @test '@LogClass and @LogMethod'(): void {

    const logSpy = sinon.spy();

    CONFIG.console = { log: logSpy } as any;
    const log      = new LogDemo('parameter1', 'parameter2');

    expect(logSpy.called).to.be.true;

    expect(log).to.be.instanceof(LogDemo);

    expect(log.parameter1).to.be.equal('parameter1');
    expect(log.parameter2).to.be.equal('parameter2');

    log.foo('parameter1', 'parameter2');

    expect(logSpy.callCount).eq(2);

    CONFIG.console = console;

  }

  @test '@LogMethod normal console'(): void {

    const logSpy = sinon.spy();

    const log = new LogUseNormalConsoleDemo('parameter1', 'parameter2');

    CONFIG.console = { log: logSpy } as any;
    log.foo('parameter1', 'parameter2');

    expect(logSpy.called).to.be.true;

    CONFIG.console = console;
  }

}
