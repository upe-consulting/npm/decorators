/* tslint:disable:no-unused-variable */
/* tslint:disable:no-unused-expression */
import { expect } from 'chai';
import { suite, test } from 'mocha-typescript';
import { SingletonError } from '../../src/exceptions/singleton.error';

@suite
class SingletonErrorTest {

  @test 'parameter less construction'(): void {
    expect(() => { throw new SingletonError(); })
      .to.throw('Can not create multiple instances of Unknown, because this class is a singleton!');
  }

  @test 'parameter construction'(): void {
    expect(() => { throw new SingletonError('MyClass'); })
      .to.throw('Can not create multiple instances of MyClass, because this class is a singleton!');
  }

}
